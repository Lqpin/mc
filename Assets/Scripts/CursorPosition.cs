﻿using UnityEngine;
using System.Collections;

public class CursorPosition : MonoBehaviour {
	private Plane plane;
	private float distance;
	private Ray ray;
	private Vector3 hitPoint;
	// Use this for initialization
	void Start () {
		Screen.showCursor = false;
		plane = new Plane(Vector3.forward, Vector3.zero);
	}
	
	// Update is called once per frame
	void Update () {
		ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		// plane.Raycast returns the distance from the ray start to the hit point
		if (plane.Raycast(ray, out distance))
		{
			// some point of the plane was hit - get its coordinates
			hitPoint = ray.GetPoint(distance);
			// use the hitPoint to aim your cannon
		}
		transform.position = hitPoint;
	}
}