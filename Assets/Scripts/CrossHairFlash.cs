﻿using UnityEngine;
using System.Collections;

public class CrossHairFlash : MonoBehaviour {
	public float flashTime=.5f;


	// Use this for initialization
	void Start () {
		Destroy (gameObject, flashTime);
		StartCoroutine(	Flash());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator  Flash(){
			//Flash Crosshair and lineRenderer
		renderer.material.color = Color.gray;

		yield return new WaitForSeconds (0.05f);
		renderer.material.color = Color.black;

		yield return new WaitForSeconds (0.05f);
		renderer.material.color = Color.gray;

		yield return new WaitForSeconds (0.05f);
		renderer.material.color = Color.black;
		
		yield return new WaitForSeconds (0.05f);
		renderer.material.color = Color.gray;
		
		yield return new WaitForSeconds (0.05f);
		renderer.material.color = Color.black;
		
		yield return new WaitForSeconds (0.05f);
		renderer.material.color = Color.gray;
		
		yield return new WaitForSeconds (0.05f);
		renderer.material.color = Color.black;
		
		yield return new WaitForSeconds (0.05f);
		renderer.material.color = Color.gray;
		
		yield return new WaitForSeconds (0.05f);
		renderer.material.color = Color.black;



	}
	
}