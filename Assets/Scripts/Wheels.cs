﻿using UnityEngine;
using System.Collections;

public class Wheels : MonoBehaviour {
	public HingeJoint2D hingy;
	public float speed =50f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		//Add torque to wheels with horizontal controls
		float h = Input.GetAxis("Horizontal");
		rigidbody2D.AddTorque(-h*speed);

		//Brake with vertical axis
		float v = Input.GetAxis("Vertical");
		if (Mathf.Abs(rigidbody2D.angularVelocity)>0){//&& v<0){ //Without this part, vertical axis up is a turbo
			rigidbody2D.AddTorque(v*1.2f*speed*Mathf.Sign(rigidbody2D.angularVelocity));
		}


//		if (Mathf.Abs (h)<0.5) 
//			hingy.useMotor = false;
//		if (Mathf.Abs (h)>0.5)
//			hingy.useMotor = true;
		//rigidbody2D.AddTorque(250);
		//hingeJoint2D.motor.motorSpeed = h*300;
//		JointMotor2D motorw;
//		motorw = hingy.motor;
//		motorw.motorSpeed = h*1000;
//		hingy.motor = motorw;

	}
}
