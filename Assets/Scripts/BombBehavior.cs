﻿using UnityEngine;
using System.Collections;

public class BombBehavior : MonoBehaviour {
	public Transform explosionPrefab;
	public Vector3 detonationPosition;
	private bool hasDetonated = true;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Destroy(gameObject,10);
		Vector3 detonationDistance = detonationPosition-transform.position;
		if (detonationDistance.magnitude<0.2f && hasDetonated){
			Explode();
			hasDetonated = false;
		}

		//Something to have the trail fade away. Does not work, needs another solution
		//Component trail = GetComponent<TrailRenderer>();
		//trail.renderer.material.color = Color.Lerp (Color.red, Color.green, Time.time);
		//trail.renderer.material.SetColor("_TintColor",Color.red);

	}

	void OnCollisionEnter2D(Collision2D collision) {

		Explode ();
	}



	public void Explode()
	{

		Transform Explosion;
		Explosion = Instantiate(explosionPrefab, transform.position, transform.rotation) as Transform;
		
		Destroy(renderer);
		Destroy(collider2D);
		Destroy(rigidbody2D);
		Destroy (audio);
	}


}
