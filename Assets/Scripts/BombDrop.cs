﻿
using UnityEngine;
using System.Collections;

public class BombDrop : MonoBehaviour {
	public GameObject Bomb;
	public float bombSpeed = 10000;
	public float bombDelay = 3f;


	private bool canLaunch = false;

	// Use this for initialization
	void Start () {
		StartCoroutine( DelayBombs());
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void SpawnBomb(){
		//Randomize launchVector (between 250° and 290° on the z axis)
		Quaternion bombLaunchRotation = Quaternion.identity;
		bombLaunchRotation.eulerAngles = new Vector3(0, 0, Random.Range(250f,290f));;
		 
		//Randomize launchPosition
		Vector3 launchPosition;
		launchPosition = new Vector3(Random.Range(-30.0f, 40.0f),transform.position.y,transform.position.z);

		//Instantiate Bomb
		GameObject clone;
		clone = Instantiate(Bomb, launchPosition , bombLaunchRotation) as GameObject;
		//Apply force to Bomb
		Vector2 bombDirection;
		bombDirection = bombLaunchRotation * Vector3.right;
		clone.rigidbody2D.AddForce(bombDirection * bombSpeed);
	}

	IEnumerator  DelayBombs(){//Delay between bombs

		while (true){
			yield return new WaitForSeconds (Random.Range (.8f,bombDelay));
			SpawnBomb();
		}


		
	}
}
