﻿using UnityEngine;
using System.Collections;

public class Canon : MonoBehaviour {
	public Transform target;
	private Vector3 lookAtVector;
	private float angle;
	private int missileCount = 10;
	public GameObject Missile;
	public float missileSpeed = 10;
	private bool readyToFire = true;
	public AudioClip[] canonShoot;
	public Rigidbody2D kickBackBody;
	private Quaternion newRotation;
	private Component[] muzzleSmoke ;
	public LineRenderer lineRenderer;
	public GameObject crossHair;
	public GameObject flashCrossHair;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.P))
		{
			Debug.Break();
		}

		//Make the Canon look at the cursor //All this because transform.LookAt works only for z vector.
		lookAtVector = target.position - transform.position;
		angle = Mathf.Atan2(lookAtVector.y, lookAtVector.x) * Mathf.Rad2Deg;
		newRotation.eulerAngles	= new Vector3(0,0,angle);
		//if (angle > 0){ //prevent the canon from looking down
			transform.rotation = newRotation;
		//}

		//Calculate angle of Canon (for some reason)
//		Vector2 angles;
//		angles = new Vector2(newRotation.eulerAngles.z,kickBackBody.transform.rotation.eulerAngles.z);




		//Set LineRenderer positions
		LineRenderer lineRenderer = gameObject.GetComponent<LineRenderer>();//putting that line outside of Update() doesnt seem to work
		lineRenderer.SetPosition(0, transform.position);
		lineRenderer.SetPosition(1, target.position);

		if (Input.GetButtonDown("Fire1")&& readyToFire)// && missileCount>0 && readyToFire)
		{
			//Instantiate Missile
			GameObject clone;
			clone = Instantiate(Missile, transform.position , transform.rotation) as GameObject;
			Vector2 missileDirection;
			missileDirection = new Vector2(lookAtVector.normalized.x,lookAtVector.normalized.y);
			clone.rigidbody2D.AddForce(missileDirection * missileSpeed);

			//Explode at cursor click
			MissileBehavior missileScript =  (MissileBehavior) clone.GetComponent(typeof(MissileBehavior)); 
			missileScript.detonationPosition = target.position;


			//Creates smoke
			muzzleSmoke = GetComponentsInChildren<ParticleSystem>();
			foreach(ParticleSystem particleSys in muzzleSmoke){
			particleSys.Play();//Smoke
			}

			//Play shoot sound
			audio.clip = canonShoot[Random.Range(0, canonShoot.Length)];
			audio.Play();

			//Make a flashy cursor at position
			GameObject flashCrossHairClone;
			Vector3 flashCrossHairClonePosition;
			flashCrossHairClonePosition = target.position;
			flashCrossHairClonePosition.z -= 1;
			flashCrossHairClone = Instantiate (flashCrossHair, flashCrossHairClonePosition, target.rotation) as GameObject;

			//Recoil to the Tank
			kickBackBody.AddForceAtPosition(missileDirection*-1000,transform.position);//Kickback




			//Delay between shots
			StartCoroutine(WaitFire());//Delay between two shots
		}

		//Debug.Log(readyToFire);
	}

	IEnumerator  WaitFire(){//Delay for muzzle light off, also between two shots
		readyToFire=false;//prevent shot

//		//Flash Crosshair and lineRenderer
//		crossHair.renderer.material.color = Color.gray;
//		yield return new WaitForSeconds (0.05f);
//		crossHair.renderer.material.color = Color.white;
//		yield return new WaitForSeconds (0.05f);
//		crossHair.renderer.material.color = Color.gray;
//		yield return new WaitForSeconds (0.05f);
//		crossHair.renderer.material.color = Color.white;


		yield return new WaitForSeconds (0.5f);
		readyToFire=true;//Allow shot
		
	}


		


}
