﻿using UnityEngine;
using System.Collections;

public class LevelWarp : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D collidingObject)
	{
		//Get the parent of the colliding object if any (mainly for the player object)



		Vector3 warpPos = collidingObject.transform.root.position; Debug.Log (warpPos);
		Vector2 warpVelocity = collidingObject.rigidbody2D.velocity ;//For missiles, transfer velocity to the other side
		//Do we want to transfer missiles? They are supposed to explode on click position


		warpPos.x *= -1.8f;//Not -1 otherwise it teleports into the other teleporter back and forth indefinitly
		Debug.Log (warpPos);

		collidingObject.transform.root.Translate (warpPos,Space.World);


		//Instantiate the object
		//GameObject clone;

		//clone = Instantiate(collidingObject.transform.root.gameObject, warpPos *-1f , collidingObject.transform.root.rotation) as GameObject;
		//clone.rigidbody2D.velocity = warpVelocity ;
		//Destroy (collidingObject);

	}
}
