﻿using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour {
	public AudioClip[] explosion;
	// Use this for initialization
	void Start () {
		audio.clip = explosion[Random.Range(0, explosion.Length)];
		audio.Play();
	}
	
	// Update is called once per frame
	void Update () {
		Destroy(gameObject,5);
	}
}
